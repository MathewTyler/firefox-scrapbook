
var sbOutputService = {

	depth : 0,
	content : "",
	isAuto : false,
	optionAll   : true,
	optionFrame : false,
	optionOpen   : true,

	/**
	 * window.arguments[0]: true means is auto mode
	 */
	init : function()
	{
		if (window.arguments && window.arguments[0]) this.isAuto = true;
		document.documentElement.getButton("accept").label = sbCommonUtils.lang("scrapbook", "START_BUTTON");
		sbTreeHandler.init(true);
		this.selectAllFolders();
		if ( this.isAuto ) this.start();
	},

	selectAllFolders : function()
	{
		if ( document.getElementById('ScrapBookOutputOptionA').checked )
		{
			sbTreeHandler.toggleAllFolders(true);
			sbTreeHandler.TREE.view.selection.selectAll();
			sbTreeHandler.TREE.treeBoxObject.focused = true;
		}
	},

	toggleAllSelection : function()
	{
		document.getElementById("ScrapBookOutputOptionA").checked = false;
	},

	start : function()
	{
		this.optionAll = document.getElementById("ScrapBookOutputOptionA").checked;
		this.optionFrame = document.getElementById("ScrapBookOutputOptionF").checked;
		this.optionOpen = document.getElementById("ScrapBookOutputOptionO").checked;
		if ( this.isAuto ) {
			this.optionOpen = false;
		}
		this.optionAll ? this.execAll() : this.exec();
		sbTreeHandler.toggleAllFolders(true);
		if ( this.isAuto ) window.close();
	},

	execAll : function()
	{
		this.content = this.getHTMLHead();
		this.processRescursively(sbTreeHandler.TREE.resource);
		this.finalize();
	},

	exec : function()
	{
		this.content = this.getHTMLHead();
		var selResList = sbTreeHandler.getSelection(true, 1);
		this.content += "<ul>\n";
		for ( var i = 0; i < selResList.length; i++ )
		{
			this.content += '<li class="depth' + String(this.depth) + '">';
			this.content += this.getHTMLBody(selResList[i]);
			this.processRescursively(selResList[i]);
			this.content += "</li>\n";
		}
		this.content += "</ul>\n";
		this.finalize();
	},

	finalize : function()
	{
		var dir = sbCommonUtils.getScrapBookDir().clone();
		dir.append("tree");
		if ( !dir.exists() ) dir.create(dir.DIRECTORY_TYPE, 0700);
		var urlHash = {
			"chrome://scrapbook/skin/output.css"     : "output.css",
			"chrome://scrapbook/skin/treeitem.png"   : "treeitem.png",
			"chrome://scrapbook/skin/treenote.png"   : "treenote.png",
			"chrome://scrapbook/skin/treenotex.png"  : "treenotex.png",
			"chrome://scrapbook/skin/treefolder.png" : "folder.png",
			"chrome://scrapbook/skin/toolbar_toggle.png" : "toggle.png",
		};
		for ( var url in urlHash )
		{
			var destFile = dir.clone();
			destFile.append(urlHash[url]);
			sbCommonUtils.saveTemplateFile(url, destFile);
		}
		var frameFile = dir.clone();
		frameFile.append("frame.html");
		if ( !frameFile.exists() ) frameFile.create(frameFile.NORMAL_FILE_TYPE, 0666);
		sbCommonUtils.writeFile(frameFile, this.getHTMLFrame(), "UTF-8");
		var indexFile = dir.clone();
		indexFile.append("index.html");
		if ( !indexFile.exists() ) indexFile.create(indexFile.NORMAL_FILE_TYPE, 0666);
		this.content += this.getHTMLFoot();
		sbCommonUtils.writeFile(indexFile, this.content, "UTF-8");
		var fileName = this.optionFrame ? "frame.html" : "index.html";
		if ( this.optionOpen )
		{
			sbCommonUtils.loadURL(sbCommonUtils.convertFilePathToURL(dir.path) + fileName, true);
		}
	},

	processRescursively : function(aContRes)
	{
		this.depth++;
		var id = sbDataSource.getProperty(aContRes, "id") || "root";
		this.content += '<ul id="folder-' + id + '">\n';
		var resList = sbDataSource.flattenResources(aContRes, 0, false);
		for (var i = 1; i < resList.length; i++) {
			this.content += '<li class="depth' + String(this.depth) + '">';
			this.content += this.getHTMLBody(resList[i]);
			if (sbDataSource.isContainer(resList[i]))
				this.processRescursively(resList[i]);
			this.content += "</li>\n";
		}
		this.content += "</ul>\n";
		this.depth--;
	},

	getHTMLHead : function()
	{
		var HTML = '<!DOCTYPE html>\n'
			+ '<html>\n\n'
			+ '<head>\n'
			+ '	<meta charset="UTF-8">\n'
			+ '	<title>' + sbCommonUtils.escapeHTML(document.title, true) + '</title>\n'
			+ '	<meta name="viewport" content="width=device-width">\n'
			+ '	<link rel="stylesheet" type="text/css" href="output.css" media="all">\n'
			+ '	<script>\n'
			+ '	function init() {\n'
			+ '		toggleAll(false);\n'
			+ '		loadHash();\n'
			+ '		registerRenewHash();\n'
			+ '	}\n'
			+ '	function loadHash() {\n'
			+ '		var hash = top.location.hash;\n'
			+ '		if (!hash) return;\n'
			+ '		hash = hash.substring(1);\n'
			+ '		if (self != top) top.frames[1].location = hash;\n'
			+ '		var aElems = document.getElementsByTagName("A");\n'
			+ '		for ( var i = 1; i < aElems.length; i++ ) {\n'
            + '		    var aElem = aElems[i];\n'
			+ '			if (aElem.getAttribute("href") == hash) {\n'
			+ '				if (self != top) top.document.title = aElem.childNodes[1].nodeValue;\n'
			+ '				var ansc = aElem.parentNode;\n'
			+ '				while (ansc) { if (ansc.nodeName == "UL") ansc.style.display = "block"; ansc = ansc.parentNode; }\n'
			+ '				aElem.focus();\n'
			+ '				break;\n'
			+ '			}\n'
			+ '		}\n'
			+ '	}\n'
			+ '	function registerRenewHash() {\n'
			+ '		var aElems = document.getElementsByTagName("A");\n'
			+ '		for ( var i = 1; i < aElems.length; i++ ) {\n'
			+ '			if (aElems[i].className != "folder") {\n'
			+ '				aElems[i].onclick = renewHash;\n'
			+ '			}\n'
			+ '		}\n'
			+ '	}\n'
			+ '	function renewHash() {\n'
			+ '		if (self == top) return;\n'
			+ '		var hash = "#" + this.getAttribute("href");\n'
			+ '		var title = this.childNodes[1].nodeValue;\n'
			+ '		if (history && history.pushState) top.history.pushState("", title, hash);\n'
			+ '		else top.location.hash = hash;\n'
			+ '		top.document.title = title;\n'
			+ '	}\n'
			+ '	function toggle(aID) {\n'
			+ '		var listElt = document.getElementById(aID);\n'
			+ '		listElt.style.display = ( listElt.style.display == "none" ) ? "block" : "none";\n'
			+ '	}\n'
			+ '	function toggleAll(willOpen) {\n'
			+ '		var ulElems = document.getElementsByTagName("UL");\n'
			+ '		if (willOpen === undefined) {\n'
			+ '			willOpen = false;\n'
			+ '			for ( var i = 1; i < ulElems.length; i++ ) {\n'
			+ '				if (ulElems[i].style.display == "none") { willOpen = true; break; }\n'
			+ '			}\n'
			+ '		}\n'
			+ '		for ( var i = 1; i < ulElems.length; i++ ) {\n'
			+ '			ulElems[i].style.display = willOpen ? "block" : "none";\n'
			+ '		}\n'
			+ '	}\n'
			+ '	</script>\n'
			+ '</head>\n\n'
			+ '<body onload="init();">\n'
			+ '<div id="header"><a href="javascript:toggleAll();">ScrapBook</a></div>\n'
		return HTML;
	},

	getHTMLBody : function(aRes)
	{
		var id    = sbDataSource.getProperty(aRes, "id");
		var type  = sbDataSource.getProperty(aRes, "type");
		var icon  = sbDataSource.getProperty(aRes, "icon");
		var title = sbDataSource.getProperty(aRes, "title");
		var source = sbDataSource.getProperty(aRes, "source");
		if ( icon.match(/(\/data\/\d{14}\/.*$)/) ) icon = ".." + RegExp.$1;
		if ( !icon ) icon = sbCommonUtils.escapeFileName(sbCommonUtils.getFileName( sbCommonUtils.getDefaultIcon(type) ));
		icon = sbCommonUtils.escapeHTML(icon);
		title = sbCommonUtils.escapeHTML(title);
		source = sbCommonUtils.escapeHTML(source);
		var ret;
		switch (type) {
			case "separator": 
				ret = '<a title="' + title + '">' + title + ' ----------------------------------------' + '</a>';
				break;
			case "folder": 
				ret = '<a class="folder" href="javascript:toggle(\'folder-' + id + '\');" title="' + title + '">'
				    + '<img src="folder.png" width="16" height="16" alt="">' + title + '</a>\n';
				break;
			case "bookmark": 
				ret = '<a href="' + source + '" target="_blank" class="' + type + '" title="' + title + '">'
				    + '<img src="' + icon + '" width="16" height="16" alt="">' + title + '</a>';
				break;
			default: 
				var href = sbCommonUtils.escapeHTML("../data/" + id + "/index.html");
				var target = this.optionFrame ? ' target="main"' : "";
				ret = '<a href="' + href + '"' + target + ' class="' + type + '" title="' + title + '">'
				    + '<img src="' + icon + '" width="16" height="16" alt="">' + title + '</a>';
				if (!source) break;
				ret += ' <a href="' + source + '" target="_blank" class="bookmark" title="Source">➤</a>';
				break;
		}
		return ret;
	},

	getHTMLFoot : function()
	{
		var HTML = "</body>\n\n</html>\n";
		return HTML;
	},

	getHTMLFrame : function()
	{
		var HTML = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">\n'
			+ '<html>\n'
			+ '<head>\n'
			+ '	<meta charset="UTF-8">\n'
			+ '	<title>' + sbCommonUtils.escapeHTML(document.title, true) + '</title>\n'
			+ '</head>\n'
			+ '<frameset cols="200,*">\n'
			+ '	<frame name="side" src="index.html">\n'
			+ '	<frame name="main">\n'
			+ '</frameset>\n'
			+ '</html>\n';
		return HTML;
	},

};



